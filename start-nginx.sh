#!/usr/bin/with-contenv sh
set -e;

/bin/wait-for-it.sh -t 120 127.0.0.1:9000

if [ -z "$PORT" ]
then
  # Default nginx port is 8080 but can be changed to to variable $PORT
  PORT=8080
fi

sed -i "s/listen 8080/listen ${PORT}/g" /etc/nginx/nginx.conf
echo "nginx port is opening at ${PORT}"

/usr/sbin/nginx
